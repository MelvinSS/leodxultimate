# LEODXULTIMATE

This is a sketch for Arduino Leonardo based IIDX controller with 9 buttons and one rotary encoder for turntable. It is a modification of iivx by 4yn (https://github.com/4yn/iivx) with added support for WS2812B RGB LED Strips and adapted for use with IIDX Infinitas and IIDX Ultimate Mobile (Android only, you'll need to know how to change the device's PID and VID to disguise the board as a IIDX Premium controller).

Pinout used for buttons and lights is based on Arduino Pro Micro layout and doesn't use pins that aren't available on it.


## Dependencies

The sketch requires Adafruit NeoPixel library to be installed.



## Features

Several extra features implemented in the sketch :

1. Switch between digital and analog turntable modes
2. Cycle between 7 colors of button LED
3. RGB turntable ring
4. Save color configurations and mode setting in EEPROM
5. Configuration via serial port
6. Individually configurable button leds

## Configuration

The code is configurable on the main ino program. The configurable parameters are inside the CONFIG PARAM section.

## ToDo

Planned features that might be implemented in the future :

1. Stepper motor support for adjustable turntable resistance
2. On-board configuration interface
3. Add two more buttons for infinitas support

Some other things that needs to be done :

- Code cleanup and reorganization
- Write a proper readme file

---