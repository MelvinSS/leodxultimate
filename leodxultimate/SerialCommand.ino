void handleSerialCommands() {
  if (serialConfigMode == true && Serial.available()) {
    String getCommand = readSerial();
    handleCommand(getCommand);
    //getCommand = "";
  }
  else {
    if (Serial.available()) {
      char charDetect = Serial.read();
      if (charDetect == 'C') serialConfigMode = true;
      while (Serial.available()) readSerial();
    }
  }
}

String readSerial() {
  String retstring = "";
  int i = 0;
  bool endString = false;
  while (Serial.available() || endString == false) {
    char readChar = Serial.read();
    retstring += readChar;
    if (readChar == '\n') endString = true;
  }
  return retstring;
}

void handleCommand(String command) {
  String key, value;
  if (command.indexOf('=') >= 0) {
    key = command.substring(0, command.indexOf('='));
    value = command.substring(command.indexOf('=') + 1, command.indexOf('\n'));
  }
  else {
    key = command.substring(0, command.indexOf('\n'));
    value = "";
  }
  //Serial.println("Key: " + key);
  //Serial.println("Value: " + value);
  doCommand(key, value);
}

void doCommand(String key, String value) {
  settingsChanged = true;
  lastEEPROMSave = millis();
  if (key == "END") {
    serialConfigMode = false;
  }
  else if (key == "BCOL") { // set button color value = <button number>,<R>,<G>,<B>
    if (value.length() > 0) {
      int bnum = value.substring(0, value.indexOf(',')).toInt() % BLED_COUNT;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int rval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int gval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int bval = value.toInt() % 256;
      buttonLedRed[bnum] = rval;
      buttonLedGreen[bnum] = gval;
      buttonLedBlue[bnum] = bval;
    }
  }
  else if (key == "TCOL") { // set turntable color
    if (value.length() > 0) {
      int rval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int gval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int bval = value.toInt() % 256;
      ttLedRGB[0] = rval;
      ttLedRGB[1] = gval;
      ttLedRGB[2] = bval;
    }
  }
  else if (key == "TCUP") { // set turntable color digital up
    if (value.length() > 0) {
      int rval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int gval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int bval = value.toInt() % 256;
      ttLedUp[0] = rval;
      ttLedUp[1] = gval;
      ttLedUp[2] = bval;
    }
  }
  else if (key == "TCDN") { // set turntable color digital down
    if (value.length() > 0) {
      int rval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int gval = value.substring(0, value.indexOf(',')).toInt() % 256;
      value = value.substring(value.indexOf(',') + 1, value.length());
      int bval = value.toInt() % 256;
      ttLedDown[0] = rval;
      ttLedDown[1] = gval;
      ttLedDown[2] = bval;
    }
  }
  else if (key == "BLMD") { // set button light mode
    if (value.length() > 0) {
      lightMode = value.toInt() % 2;
    }

  }
  else if (key == "TTMD") { //set turntable mode
    if (value.length() > 0) {
      ttMode = value.toInt() % 2;
    }
  }
  else if (key == "BTLS") {
    if (value.length() > 0) {
      buttonLedSelect = value.toInt() % BLED_SEL_LIMIT;
    }
  }
  else if (key == "TTLS") {
    if (value.length() > 0) {
      ttLedSelect = value.toInt() % TLED_SEL_LIMIT;
    }
  }
  else if (key == "TTSM") {
    if (value.length() > 0) {
      ttSensMultiplier = (double)(value.toInt() % TTSENSMUL_SEL_LIMIT);
      setTTSensitivityValue();
    }
  }
  else if (key == "TTSC") {
    if (value.length() > 0) {
      ttSensCustomValue = value.toDouble();
      setTTSensitivityValue();
    }
  }
  else if (key == "FWVR") {
    Serial.println("FWVR=" + String(FWVER));
  }
  else if (key == "BRID") {
    Serial.println("BRID=" + String(BOARDID));
  }
  else if (key == "TEST") { //test light for 3 seconds
    int testDuration = 0;
    if (value.length() > 0) {
      testDuration = value.toInt();
      if (testDuration > 10000) testDuration = 10000;
    }
    else {
      testDuration = 3000;
    }
    showButton();
    delay(testDuration);
  }
  else if (key == "STAT") { // get currentsettings
    //Serial.println("FWVR=" + String(FWVER));
    Serial.println("BLMD=" + String(lightMode)); // Button light mode
    Serial.println("TTMD=" + String(ttMode)); // TT mode
    Serial.println("BTLS=" + String(buttonLedSelect)); //Button light select
    Serial.println("TTLS=" + String(ttLedSelect)); //TT light select
    for (int i = 0; i < BLED_COUNT; i++) {
      Serial.println("BCOL=" + String(i) + "," + String(buttonLedRed[i]) + "," + String(buttonLedGreen[i]) + "," + String(buttonLedBlue[i]));
    }
    Serial.println("TCOL=" + String(ttLedRGB[0]) + "," + String(ttLedRGB[1]) + "," + String(ttLedRGB[2]));
    Serial.println("TCUP=" + String(ttLedUp[0]) + "," + String(ttLedUp[1]) + "," + String(ttLedUp[2]));
    Serial.println("TCDN=" + String(ttLedDown[0]) + "," + String(ttLedDown[1]) + "," + String(ttLedDown[2]));
    Serial.println("TTSC=" + String(ttSensCustomValue, 3));
    Serial.println("TTSM=" + String(ttSensMultiplier));
  }

}

void printDebug(String message) {
  if (DEBUG_MODE == 1) {
    Serial.println("[DEBUG] " + message);
  }
}

