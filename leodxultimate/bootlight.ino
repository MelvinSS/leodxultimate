int bootscrollpoint = 0;
unsigned long animationdelay = 0;

void bootlight() {
  unsigned long bootstarttime = millis();
  while (millis() - bootstarttime < 2000) {
    if (millis() - animationdelay > 2) {
      bootscrollpoint += 1;
      setRainbowBoot(bootscrollpoint % 255);
      setRainbowBootButton(bootscrollpoint % 255);
      animationdelay = millis();
    }
  }
}

void setRainbowBoot(uint16_t point) {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, Wheel((i * 256 / strip.numPixels() + point) & 255));
  }
  strip.show();

}

void setRainbowBootButton(uint16_t point) {
  for (uint16_t i = 0; i < 7; i++) {
    buttonleds.setPixelColor(i, Wheel(((i * 256 / 18) + point) & 255));
    if (i == 0) {
      buttonleds.setPixelColor(7, Wheel(((i * 256 / 18) + point) & 255));
    }
    else if (i == 6) {
      buttonleds.setPixelColor(8, Wheel(((i * 256 / 18) + point) & 255));
    }
  }
  buttonleds.show();
}


