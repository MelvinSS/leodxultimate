void readButtons() {
  // Read buttons

  for (int i = 0; i < buttonCount; i++) {
    int j = i;
    if (j == 7) j++;
    if (digitalRead(buttonPins[i]) != HIGH) {
      report.buttons |= (uint16_t)1 << j;
    } else {
      report.buttons &= ~((uint16_t)1 << j);
    }
  }
}

void readCombination() {
  if ((digitalRead(sysPin1) == HIGH) && (digitalRead(sysPin2) != HIGH)) {
    if (digitalRead(buttonPins[0]) != HIGH) {
      report.buttons |= (uint16_t)1 << buttonCount;
      report.buttons &= ~((uint16_t)1 << 0);
      report.buttons &= ~((uint16_t)1 << 9);
    }
    else {
      report.buttons &= ~((uint16_t)1 << buttonCount);
    }
    if (digitalRead(buttonPins[2]) != HIGH) {
      report.buttons |= (uint16_t)1 << buttonCount + 1;
      report.buttons &= ~((uint16_t)1 << 2);
      report.buttons &= ~((uint16_t)1 << 9);
    }
    else {
      report.buttons &= ~((uint16_t)1 << buttonCount + 1);
    }
    if (digitalRead(buttonPins[4]) != HIGH) {
      report.buttons |= (uint16_t)1 << buttonCount;
      report.buttons |= (uint16_t)1 << 9;
      report.buttons &= ~((uint16_t)1 << 4);
    }
    else {
      if (digitalRead(buttonPins[0]) == HIGH) {
        report.buttons &= ~((uint16_t)1 << buttonCount);
      }
    }
  }
  else {
    report.buttons &= ~((uint16_t)1 << buttonCount);
    report.buttons &= ~((uint16_t)1 << buttonCount + 1);
  }
}

void readTurntable() {
  encL = encoderL.read();
  if (ttMode == 1) { //digital
    // Read turntable buttons
    if ( (int32_t)(encL / ENCODER_SENSITIVITY) - encLlast > 6) {
      if (millis() - encLmillis > ENCODER_L_MILLIS_TOLERANCE || bitRead(report.buttons, 9)) {
        setStaticUpDown(1);
        report.xAxis = 255;
        encLlast = (encL / ENCODER_SENSITIVITY);
        encLmillis = millis();
      }
    }
    else if ( (int32_t)(encL / ENCODER_SENSITIVITY) - encLlast < -6) {
      if (millis() - encLmillis > ENCODER_L_MILLIS_TOLERANCE || bitRead(report.buttons, 10)) {
        setStaticUpDown(2);
        report.xAxis = 0;
        encLlast = (encL / ENCODER_SENSITIVITY);
        encLmillis = millis();
      }
    }
    else {
      if (millis() - encLmillis > ENCODER_L_MILLIS_TOLERANCE_NEUTRAL) {
        //scrollpoint += 1;
        setTurntableColor(ttLedSelect);
        report.xAxis = 127;
      }
    }
  }
  else { //analog mode
    // Read Encoders
    report.xAxis = (uint8_t)((int32_t)(encL / ENCODER_SENSITIVITY) % 256);
    setTurntableColorAnalog(ttLedSelect);
  }
  report.yAxis = (uint8_t) 127;
  report.zAxis = (uint8_t) 127;
}


void setTurntableColor(int colors) {
  if (colors == 8 || colors == 9) {
    setRainbow(scrollpoint % 255);
  }
  else {
    setStaticTurntable(colors);
  }
}

void setTurntableColorAnalog(int colors) {
  if (colors == 8 || colors == 9) {
    setRainbow(report.xAxis * strip.numPixels() / stripsensdiv);
  }
  else {
    setStaticTurntable(colors);
  }
}

void controlLights() {
  // Light LEDs
  if (serialConfigMode) {
    lights(0xfff);
  }
  else if (lightMode == 0) {
    uint16_t sevenkeyRepSeg = report.buttons & 0x007f;
    uint16_t auxkeyRepSeg = report.buttons & 0x0f80;
    auxkeyRepSeg = auxkeyRepSeg >> 1;
    uint16_t finalreport = sevenkeyRepSeg + auxkeyRepSeg;
    //lights(report.buttons);
    lights(finalreport);
  }
  else {
    lights(iivx_led);
  }
}

void handleButtonColorSetting() {
  for (int i = 0; i < buttonCount; i++) {
    if (digitalRead(buttonPins[i]) != HIGH) {

    } else {

    }
  }
}

void showButton() {
  lights(0xfff);
}

