#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>
#include "iivx_leo.h"
#include "Encoder.h"
iivxReport_t report;

#define DEBUG_MODE 0
#define REPORT_DELAY 2000
// Number of microseconds between HID reports
// 2000 = 500hz
#define FWVER "1.01"
#define BOARDID 0
#define BLED_SEL_LIMIT 10
#define TLED_SEL_LIMIT 10
#define TTSENSMUL_SEL_LIMIT 9

//=======================CONFIG PARAMS=======================
#define ENC_L_A 0 // Arduino pin connected to the encoder's output
#define ENC_L_B 1 // Arduino pin connected to the encoder's output
#define ENCODER_SENSITIVITY_STEP (double) 0.125 // Defines the interval of TT sensitivity preset 
//#define ENCODER_SENSITIVITY (double) 0.375 // Encoder's PPR divider. Higher value means bigger division which means less sensitivity.
#define ENCODER_L_MILLIS_TOLERANCE 10 // Amount of miliseconds to wait and change state of turntable buttons. Only for digital TT mode.
#define ENCODER_L_MILLIS_TOLERANCE_NEUTRAL 300 // How long the TT will be held on non neutral position. Only for digital TT mode.
#define PIXEL_COUNT 12 // Turntable light. How many pixels the WS2812B has.
#define PIXEL_PIN 18 // Turntable light data pin.
#define BLED_COUNT 9 // LED light. How many pixels the WS2812B has.
#define BLED_PIN 15 // Button light data pin.

double ENCODER_SENSITIVITY = 0.375;
uint8_t buttonCount = 10; // How many buttons your controller has. You also need to specify the pins on buttonPins variable
uint8_t buttonPins[] = {4, 5, 6, 7, 8, 9, 10, 16, 16, 14}; // This will appear as button 1 to button x
uint8_t sysPin1 = 16; // The first button to hold to change settings
uint8_t sysPin2 = 14; // The second button to hold to change settings
uint8_t sysPin3 = 7; //The third button to hold to change settings
uint8_t lightModePin = 10; // The button to press to change light mode
uint8_t ttModePin = 4; // The button to press to change TT mode
uint8_t buttonColorChangePin = 9; // The button to press to change button color
uint8_t ttColorChangePin = 5; // The button to press to change TT color 
uint8_t ttSensChangePin = 6; // The button to press to change TT sensitivity multiplier
//=======================CONFIG PARAMS=======================

Encoder encoderL(0, 1);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel buttonleds = Adafruit_NeoPixel(BLED_COUNT, BLED_PIN, NEO_GRB + NEO_KHZ800);

//=======================EEPROM ADDRESSES=======================
int lightModeMemAddress = 0;
int ttModeMemAddress = 1;
int lightColMemAddress = 2;
int ttLightColMemAddress = 3;
int sensitivityChoiceMemAddress = 4;
int customSensitivityMemAddress = 5; // This value requires two addresses
int customColMemAddress = 7;

//=======================EEPROM ADDRESSES=======================


boolean customColorMode = false; //false = Preset, true = Custom
uint8_t lightMode = 1;
uint8_t ttMode = 1;
boolean serialConfigMode = false;

int stripsensdiv = PIXEL_COUNT;

int scrollpoint = 0;
int scrollpointb = 0;
uint32_t pixelRed = strip.Color(255, 0, 0);
uint32_t pixelBlue = strip.Color(0, 0, 255);
uint32_t pixelWhite = strip.Color(255, 255, 255);
uint32_t buttonLedColor[] = {strip.Color(255, 0, 0), strip.Color(0, 255, 0), strip.Color(0, 0, 255), strip.Color(255, 255, 0), strip.Color(0, 255, 255), strip.Color(255, 0, 255), strip.Color(255, 255, 255)};
uint8_t buttonLedSelect = 0;
uint8_t ttLedSelect = 0;
uint16_t currentPixel = 0;

double ttSensCustomValue = 0.50;
double ttSensMultiplier = 1;
int btnColPreset[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int presetColor[][3] = {{255, 0, 0}, {0, 255, 0}, {0, 0, 255}, {255, 255, 0}, {0, 255, 255}, {255, 0, 255}, {255, 255, 255}};


int buttonLedRed[] = {255, 0, 255, 0, 255, 0, 255, 0, 255};
int buttonLedGreen[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int buttonLedBlue[] = {0, 255, 0, 255, 0, 255, 0, 255, 0};
int ttLedRGB[] = {0,0,255};
int ttLedUp[] = {0,0,0};
int ttLedDown[] = {0,0,0};

boolean buttonColorSettingMode = false;
boolean settingsChanged = false;

unsigned long lastPixel = 0;
unsigned long intervalPixel = 100;
unsigned long lastChangePin = 0;
unsigned long lastEEPROMSave = 0;
volatile int32_t encL = 0, encLlast = 0, encLmillis = 0;


void setup() {
   Serial.begin(115200);
  delay(1000);
  loadSettings();
  strip.begin();
  buttonleds.begin();
  strip.show();
  buttonleds.show();
  // Setup I/O for pins
  for (int i = 0; i < buttonCount; i++) {
    pinMode(buttonPins[i], INPUT_PULLUP);
  }
  pinMode(sysPin1, INPUT_PULLUP);
  pinMode(sysPin2, INPUT_PULLUP);
  //setup interrupts
  pinMode(ENC_L_A, INPUT_PULLUP);
  pinMode(ENC_L_B, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(ENC_L_A), doEncL, RISING);
  delay(100);
  bootlight();

}

void loop() {
  if (buttonColorSettingMode) handleButtonColorSetting();
  readButtons();
  readCombination();
  readTurntable();
  controlLights();
  handleSerialCommands();
  handleSettingCommand();
  if (settingsChanged) saveSettings();
  // Send report and delay
  iivx.setState(&report);
  delayMicroseconds(REPORT_DELAY);
 
  if (ttLedSelect == 8){
     scrollpoint += 1;
  }
  else if (ttLedSelect == 9){
     scrollpoint -= 1;
  }
  
  if (buttonLedSelect == 8){
      scrollpointb += 1;
  }
  else if (buttonLedSelect == 9){
      scrollpointb -= 1;
  }

}
