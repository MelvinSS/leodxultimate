void setPresetButtonColor(int btnIndex, int presetindex) {
  buttonLedRed[btnIndex] = presetColor[presetindex][0];
  buttonLedGreen[btnIndex] = presetColor[presetindex][1];
  buttonLedBlue[btnIndex] = presetColor[presetindex][2];
}

void handleSettingCommand() {
  // Detect Syspin Entries
  if ((digitalRead(sysPin1) != HIGH) && (digitalRead(sysPin2) != HIGH) && (digitalRead(sysPin3) != HIGH)) {
    if (digitalRead(lightModePin) != HIGH) {
      if (millis() - lastChangePin > 500) {
        lightMode = (lightMode + 1) % 2;
        if (serialConfigMode) Serial.println("BLMD=" + String(lightMode));
        lastEEPROMSave = millis();
        lastChangePin = millis();
        settingsChanged = true;
      }
    }
    else if (digitalRead(ttModePin) != HIGH) {
      if (millis() - lastChangePin > 500) {
        ttMode = (ttMode + 1) % 2;
        if (serialConfigMode) Serial.println("TTMD=" + String(ttMode)); // TT mode
        lastEEPROMSave = millis();
        lastChangePin = millis();
        settingsChanged = true;
      }
    }
    else if (digitalRead(buttonColorChangePin) != HIGH) {
      if (millis() - lastChangePin > 500) {
        buttonLedSelect = (buttonLedSelect + 1) % BLED_SEL_LIMIT;
        if (serialConfigMode) Serial.println("BTLS=" + String(buttonLedSelect));
        lastEEPROMSave = millis();
        lastChangePin = millis();
        settingsChanged = true;
      }
    }
    else if (digitalRead(ttColorChangePin) != HIGH) {
      if (millis() - lastChangePin > 500) {
        ttLedSelect = (ttLedSelect + 1) % TLED_SEL_LIMIT;
        if (serialConfigMode)  Serial.println("TTLS=" + String(ttLedSelect));
        lastEEPROMSave = millis();
        lastChangePin = millis();
        settingsChanged = true;
      }
    }
    else if (digitalRead(ttSensChangePin) != HIGH) {
      if (millis() - lastChangePin > 500) {
        ttSensMultiplier = ((int)ttSensMultiplier + 1) % TTSENSMUL_SEL_LIMIT;
        setTTSensitivityValue();
        if (serialConfigMode)  Serial.println("TTSM=" + String(ttSensMultiplier));
        lastEEPROMSave = millis();
        lastChangePin = millis();
        settingsChanged = true;
      }
    }
    /*
      else if (digitalRead(customColorTogglePin) != HIGH) {
      if (millis() - lastChangePin > 500) {

        customColorMode = !customColorMode;
         Serial.println(customColorMode);
        lastColorEEPROMSave = millis();
        lastChangePin = millis();
      }
      }
    */
  }
}

void saveSettings() {
  if (millis() - lastEEPROMSave > 10000) {
    printDebug("Saving config");
    EEPROM.update(lightModeMemAddress, lightMode);
    EEPROM.update(ttModeMemAddress, ttMode);
    //Serial.println("Settings Saved");
    EEPROM.update(lightColMemAddress, buttonLedSelect);
    EEPROM.update(ttLightColMemAddress, ttLedSelect);
    EEPROM.update(sensitivityChoiceMemAddress, (uint8_t)ttSensMultiplier);
    uint8_t ttSensCVWhole = (uint8_t)(ttSensCustomValue * 10);
    double ttSensCVDecimal = (ttSensCustomValue * 10) - ttSensCVWhole;
    uint8_t ttSensCVDecimalMul = (uint8_t)(ttSensCVDecimal * 100);
    EEPROM.update(customSensitivityMemAddress, ttSensCVWhole); //Save whole part
    printDebug("ttSensCVWhole Value = " + String(ttSensCVWhole));
    printDebug("ttSensCVDecimalMul Value = " + String(ttSensCVDecimalMul));
    EEPROM.update(customSensitivityMemAddress + 1, ttSensCVDecimalMul); //Save decimal part
    saveCustomColor();
    lastEEPROMSave = millis();
    settingsChanged = false;
    //Serial.println("Color Saved");
  }

}

void saveCustomColor() {
  //EEPROM.update(boardIDMemAddress, BOARDID);
  for (int i = 0; i < BLED_COUNT; i++) {
    EEPROM.update(customColMemAddress + i, buttonLedRed[i]);
  }
  for (int i = 0; i < BLED_COUNT; i++) {
    EEPROM.update(customColMemAddress + 9 + i, buttonLedGreen[i]);
  }
  for (int i = 0; i < BLED_COUNT; i++) {
    EEPROM.update(customColMemAddress + 18 + i, buttonLedBlue[i]);
  }
  for (int i = 0; i < 3; i++) {
    EEPROM.update(customColMemAddress + 27 + i, ttLedRGB[i]);
  }
  for (int i = 0; i < 3; i++) {
    EEPROM.update(customColMemAddress + 30 + i, ttLedUp[i]);
  }
  for (int i = 0; i < 3; i++) {
    EEPROM.update(customColMemAddress + 33 + i, ttLedDown[i]);
  }
}

void loadSettings() {
  ttSensMultiplier = (double)(EEPROM.read(sensitivityChoiceMemAddress) % TTSENSMUL_SEL_LIMIT);
  double TTSCdecpart = EEPROM.read(customSensitivityMemAddress + 1) / 1000.0;
  double TTSCwholepart = EEPROM.read(customSensitivityMemAddress) / 10.0;
  ttSensCustomValue = TTSCdecpart + TTSCwholepart;
  printDebug("TTSCdecpart = " + String(TTSCdecpart, 3));
  printDebug("TTSCwholepart = " + String(TTSCwholepart, 3));
  printDebug("TTSC Value = " + String(ttSensCustomValue, 3));
  if (ttSensCustomValue < 0.125 or ttSensCustomValue > 1) ttSensCustomValue = 0.50;
  setTTSensitivityValue();
  lightMode = EEPROM.read(lightModeMemAddress) % 2;
  ttMode = EEPROM.read(ttModeMemAddress) % 2;
  buttonLedSelect = EEPROM.read(lightColMemAddress) % BLED_SEL_LIMIT;
  ttLedSelect = EEPROM.read(ttLightColMemAddress) % TLED_SEL_LIMIT;
  for (int i = 0; i < BLED_COUNT; i++) {
    buttonLedRed[i] = EEPROM.read(customColMemAddress + i) % 256;
  }
  for (int i = 0; i < BLED_COUNT; i++) {
    buttonLedGreen[i] = EEPROM.read(customColMemAddress + 9 + i) % 256;
  }
  for (int i = 0; i < BLED_COUNT; i++) {
    buttonLedBlue[i] = EEPROM.read(customColMemAddress + 18 + i) % 256;
  }
  for (int i = 0; i < 3; i++) {
    ttLedRGB[i] = EEPROM.read(customColMemAddress + 27 + i) % 256;
  }
  for (int i = 0; i < 3; i++) {
    ttLedUp[i] = EEPROM.read(customColMemAddress + 30 + i) % 256;
  }
  for (int i = 0; i < 3; i++) {
    ttLedDown[i] = EEPROM.read(customColMemAddress + 33 + i) % 256;
  }
}

void setTTSensitivityValue() {
  if (ttSensMultiplier > 0) {
    ENCODER_SENSITIVITY = ttSensMultiplier * ENCODER_SENSITIVITY_STEP;
  }
  else if (ttSensMultiplier == 0) {
    ENCODER_SENSITIVITY = ttSensCustomValue;
  }
}

