uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void setPixels(uint32_t c1, uint32_t c2, uint16_t point) {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    if (i == point) strip.setPixelColor(i, c2);
    else strip.setPixelColor(i, c1);
  }
  strip.show();
}

void setRainbow(uint16_t point) {
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, Wheel((i * 256 / strip.numPixels() + point) & 255));
  }
  strip.show();
}

void setStaticTurntable(int colindex){
  for (uint16_t i = 0; i < strip.numPixels(); i++) {
    if (colindex > 6){
          strip.setPixelColor(i, strip.Color(ttLedRGB[0], ttLedRGB[1], ttLedRGB[2]));
    }
    else{
          strip.setPixelColor(i, buttonLedColor[colindex]);
    }
    

  }
  strip.show();
}

void setStaticUpDown(int updownhalf) {
  switch (updownhalf) {
    case 1:
      for (uint16_t i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, strip.Color(ttLedUp[0], ttLedUp[1], ttLedUp[2]));
      }
      break;
    case 2:
      for (uint16_t i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, strip.Color(ttLedDown[0], ttLedDown[1], ttLedDown[2]));
      }
      break;
    default:
      break;
  }
  strip.show();
}
