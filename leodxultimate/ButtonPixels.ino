void lights(uint16_t lightDesc) {
  for (int i = 0; i < BLED_COUNT ; i++) {
    
    if ((lightDesc >> i) & 1) {
      //buttonleds.setPixelColor(i, buttonLedColor[buttonLedSelect]);
      if (buttonLedSelect == 7) {
        buttonleds.setPixelColor(i, strip.Color(buttonLedRed[i], buttonLedGreen[i], buttonLedBlue[i]));
      }
      else if (buttonLedSelect == 8 || buttonLedSelect == 9) {
        //i = 8 - i;
        if (i == 7) {
          buttonleds.setPixelColor(7, Wheel(((0 * 256 / 18) + scrollpointb) & 255));
        }
        else if (i == 8) {
          buttonleds.setPixelColor(8, Wheel(((6 * 256 / 18) + scrollpointb) & 255));
        }
        else {
          buttonleds.setPixelColor(i, Wheel(((i * 256 / 18) + scrollpointb) & 255));
        }
      }
      else {
        buttonleds.setPixelColor(i, buttonLedColor[buttonLedSelect]);
      }
    } else {
      buttonleds.setPixelColor(i, strip.Color(0, 0, 0));
    }
  }
  buttonleds.show();
}

